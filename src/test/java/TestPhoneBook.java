import com.okaram.funBook.Entry;
import com.okaram.funBook.PhoneBook;
import org.junit.Assert;
import org.junit.Test;


public class TestPhoneBook {

    public Entry makeOrlandoBloom() {
        Entry e=new Entry("Orlando","Bloom");
        e.addPhoneNumber("123-456-6789","Intergalactic");
        return e;
    }

    public Entry makeTonyOrlando() {
        Entry e2=new Entry("Tony","Orlando");
        e2.addPhoneNumber("111-456-6789","Cell");
        e2.addPhoneNumber("111-456-6788","Home");
        return e2;
    }

    @Test
    public void testAllEntries() {
        PhoneBook pb=new PhoneBook();
        pb.addEntry( makeOrlandoBloom() );
        pb.addEntry( makeTonyOrlando() );

        Assert.assertEquals(2,   pb.getAllEntries().size());
    }

    @Test
    public void testEntriesByFirstName() {
        PhoneBook pb=new PhoneBook();
        pb.addEntry( makeOrlandoBloom() );
        pb.addEntry( makeOrlandoBloom() );
        pb.addEntry( makeTonyOrlando() );

        Assert.assertEquals(3,   pb.getAllEntries().size());
        Assert.assertEquals(2,   pb.getEntriesForFirstName("Orlando").size());
    }

}
