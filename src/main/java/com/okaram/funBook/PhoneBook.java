package com.okaram.funBook;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.*;

public class PhoneBook {
    private List<Entry> entryList=new ArrayList<>();
    private Map<String, Set<Entry>> entriesByFirstName=new HashMap<>();
    private Map<String, Set<Entry>> entriesByLastName= new HashMap<>();

    public List<Entry> getEntries() {
        return entryList;
    }

    public void addEntry(Entry e) {
        entryList.add(e);

        if(entriesByFirstName.containsKey(e.getFirstName())) {
            entriesByFirstName.get(e.getFirstName()).add(e);
        } else {
            Set<Entry> entriesForThisFirstName=new HashSet<>();
            entriesForThisFirstName.add(e);
            entriesByFirstName.put(e.getFirstName(), entriesForThisFirstName);
        }

        if(entriesByLastName.containsKey(e.getLastName())) {
            entriesByLastName.get(e.getLastName()).add(e);
        } else {
            Set<Entry> entriesForThisLastName=new HashSet<>();
            entriesForThisLastName.add(e);
            entriesByLastName.put(e.getLastName(), entriesForThisLastName);
        }
    }

    public Set<Entry> getEntriesForFirstName(String firstName) {
        return entriesByFirstName.get(firstName);
    }

    public Set<Entry> getEntriesForLastName(String lastName) {
        return entriesByLastName.get(lastName);
    }

    @JsonIgnore
    public Collection<Entry> getAllEntries() {
        return entryList;
    }

    public void save(String filePath) throws FileNotFoundException {
        PrintStream printStream=new PrintStream(filePath);

        for(Entry e : entryList) {
            printStream.println("Entry:");
            e.save(printStream);
        }
        printStream.println();
        printStream.close();
    }

    public void load(String filePath) throws FileNotFoundException {
        Scanner sc=new Scanner(new File(filePath));
        Entry newEntry=Entry.readFromScanner(sc);
        while(newEntry!=null) {
            addEntry(newEntry);
            newEntry=Entry.readFromScanner(sc);
        }
    }

    public void reset() {
        entryList=new ArrayList<>();
        entriesByFirstName=new HashMap<>();
        entriesByLastName= new HashMap<>();
    }
}
