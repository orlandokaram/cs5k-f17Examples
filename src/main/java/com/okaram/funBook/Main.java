package com.okaram.funBook;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.FileNotFoundException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class Main {

    static void printEntries(Collection<Entry> entries) {
        for(Entry e : entries) {
            System.out.println(e);
        }
    }
    public static Entry makeOrlandoBloom() {
        Entry e=new Entry("Orlando","Bloom");
        e.addPhoneNumber("123-456-6789","Intergalactic");
        return e;
    }

    public static Entry makeTonyOrlando() {
        Entry e2=new Entry("Tony","Orlando");
        e2.addPhoneNumber("111-456-6789","Cell");
        e2.addPhoneNumber("111-456-6788","Home");
        return e2;
    }


    public static void main(String[] args) {
        PhoneBook pb=new PhoneBook();
        pb.addEntry( makeTonyOrlando());
        pb.addEntry( makeOrlandoBloom());

        ObjectMapper objectMapper=new ObjectMapper();

        try {
            System.out.println( objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(pb) );
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

/*
        try {
//            pb.save("/Users/curri/pb.txt");
            PhoneBook pb2=new PhoneBook();

            pb2.load("/Users/curri/pb.txt");
            printEntries(  pb2.getAllEntries() );
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
*/
    }
}
