package com.okaram.funBook;


import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Entry {
    private String firstName, lastName;
    private List<PhoneNumberEntry> phoneNumbers=new ArrayList<>();

    public Entry(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public List<PhoneNumberEntry> getPhoneNumbers() {
        return phoneNumbers;
    }

    public void addPhoneNumber(String number, String type) {
        phoneNumbers.add(new PhoneNumberEntry(number,type));
    }

    @Override
    public String toString() {
        return "Entry{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", phoneNumbers=" + phoneNumbers +
                '}';
    }

    public void save(PrintStream out) {
        out.println(  firstName );
        out.println( lastName );
        for(PhoneNumberEntry entry : getPhoneNumbers()) {
            out.println(entry.getType());
            out.println(entry.getNumber());
        }
        out.println();
    }

    public static Entry readFromScanner(Scanner sc) {
        String line=sc.nextLine();
        if(!line.equals("Entry:")) {
            return null;
        }

        String firstName=sc.nextLine();
        String lastName=sc.nextLine();

        Entry e=new Entry(firstName,lastName);
        while (true) {
            String type=sc.nextLine();
            if(type.equals(""))
                break;
            String phoneNumber=sc.nextLine();
            e.addPhoneNumber(phoneNumber,type);
        }
        return e;
    }
}
