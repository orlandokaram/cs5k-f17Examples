package com.okaram;


public class ArrayExamples {

    public static void printArray(int[] arr){
        for(int i=0; i<arr.length; ++i) {
            System.out.print(arr[i]+", ");
        }
        System.out.println();
    }

    public static void printArrayFE(int[] arr){
        for(int elem : arr) {
            System.out.print(elem+", ");
        }
        System.out.println();
    }


    public static int min(int[] arr) {
        // we assume the array is not empty
        int theMin=arr[0];

        for(int i=1; i<arr.length; ++i) {
            if(arr[i]<theMin)
                theMin=arr[i];
        }

        return theMin;
    }

    public static int max(int[] arr) {
        // we assume the array is not empty
        int theMax=arr[0];

        for(int i=1; i<arr.length; ++i) {
            if(arr[i]>theMax)
                theMax=arr[i];
        }

        return theMax;
    }

    public static int dotProduct(int a1[], int a2[]) {
        int dotP=0;
        for(int i=0; i<a1.length && i<a2.length; ++i) {
            dotP += a1[i] * a2[i];
        }
        return dotP;
    }




    public static void initArray(int[] arr, int low, int step){
        for(int i=0; i<arr.length; ++i) {
            arr[i] = low + i*step;
        }
    }


    public static int[] makeArray(int low, int step, int numElems){
        int arr[]=new int[numElems];
        initArray(arr,low,step);
        return arr;
    }


    public static int[][] makeMatrix(int rows, int columns, int low, int step) {
        int [][] matrix= new int[rows][];

        for(int row=0;row<rows;++row) {
            matrix[row]=new int[columns];
            for(int col=0; col<columns; ++col) {
                matrix[row][col]=low+row*columns*step + col*step;
            }
        }

        return matrix;
    }

    public static void printMatrix(int[][]matrix) {
        for( int[] elem : matrix) {
            printArray(elem);
        }
    }

    static double addAll(double [] arr) {
        double sum=0;

        for(double elem : arr) {
            sum += elem;
        }
        return sum;
    }

    static void multiplyByValue(double[] arr, double value) {

        for(int i=0; i<arr.length; ++i) {
            arr[i]=arr[i]*value;
        }
    }

    static void multiplyOddNumberedByValue(double[] arr, double value) {
        for(int i=1; i<arr.length; i+=2) {
            arr[i]=arr[i]*value;
        }
    }
    public static void printArrayDoubles(double[] arr){
        for(double elem : arr) {
            System.out.print(elem+", ");
        }
        System.out.println();
    }


    static int minimumIndex(int [] arr, int from) {
        int minIdx=from;
        for(int i=from+1; i<arr.length; ++i) {
            if(arr[i]<arr[minIdx]) {
                minIdx=i;
            }
        }
        return minIdx;
    }

    static void swap(int[] arr, int i1, int i2) {
        int temp=arr[i1];
        arr[i1] = arr[i2];
        arr[i2] = temp;
    }

    static void selectionSort(int [] arr) {
        for(int i=0; i<arr.length; ++i) {
            int minIdx=minimumIndex(arr, i);
            swap(arr, i, minIdx);
        }
    }

    static boolean doBubble(int arr[], int to) {
        boolean swapped=false;
        for(int i=0; i<to; ++i) {
            if(arr[i]>arr[i+1]) {
                swap(arr, i, i+1);
                swapped=true;
            }
        }
        return swapped;
    }

    static void bubbleSort(int [] arr) {
        printArray(arr);
        for(int i=arr.length-1; i>1; --i) {
            boolean swapped=doBubble(arr, i);
            printArray(arr);
            if(!swapped) {
                return;
            }
        }
    }

    static void sort(int[] arr) {
        bubbleSort(arr);
    }

    static int binSearch(int arr[], int target, int low, int high) {
        int middle=(low+high)/2;
        if(low>high) {
            return -1;
        }
        if(arr[middle]==target) {
            return middle;
        }
        if(arr[middle]>target) {
            return binSearch(arr,target, low,middle-1);
        } else {
            return binSearch(arr, target, middle+1,high);
        }
    }

    static int binSearch(int arr[], int target) {
        return binSearch(arr,target, 0,arr.length-1);
    }

    public static void main(String[] args) {
        int numbers[] = {2,1,7,5,4,1,2,3};

        sort(numbers);
    }



}
