package com.okaram.io;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.Scanner;

public class IOExamples {

    public static void catFile(String filePath, PrintStream out) throws FileNotFoundException {
        FileInputStream f=new FileInputStream(filePath);
        Scanner in=new Scanner(f);
        out.println(filePath);
        out.println("----------");
        while(in.hasNext()) {
            out.println( in.nextLine() );
        }
    }


    public static void printFullPath(File f) {

        if(f!=null) {
            System.out.println(f.getName());
            printFullPath(f.getParentFile());
        }
    }

    public static void main(String[] args) {
        File f=null;

        try {
            f=new File("/Users/curri/test.txt");
        }
        catch (Exception e) {

        }
        printFullPath( f );
    }
}


