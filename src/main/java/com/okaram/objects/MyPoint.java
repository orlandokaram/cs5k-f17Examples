package com.okaram.objects;

public class MyPoint {
    private int x, y;

    public MyPoint(int x, int y) {
        this.x=x;
        this.y=y;
    }

    public int getX() {
        return this.x;
    }

    public void setX(int x) {
        this.x=x;
    }

    public void translateX(int dx) {
        this.x=this.x+dx;
    }

    public int getY() {
        return this.getY();
    }

    public double distance(MyPoint that) {
        return Math.sqrt((this.x-that.x)*(this.x-that.x) +
                (this.y-that.y)*(this.y-that.y));
    }
}
