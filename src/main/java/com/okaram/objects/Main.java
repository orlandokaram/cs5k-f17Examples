package com.okaram.objects;

public class Main {
    public static void main(String[] args) {
        MyPoint p1=new MyPoint(10,10);
        MyPoint p2=new MyPoint(20,20);

        Circle c1=new Circle(12,12,5);
        Circle c2=new Circle(15,15,1);
        Circle c3=new Circle(20,20,1);

        System.out.println(c1.intersects(c2));
        System.out.println(c1.intersects(c3));
    }
}
