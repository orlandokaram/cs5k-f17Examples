package com.okaram.objects;


public class Circle {
    int radius;
    MyPoint center;

    public Circle(int x, int y, int radius) {
        this.center=new MyPoint(x,y);
        this.radius=radius;
    }

    public MyPoint getCenter() {
        return center;
    }

    public int getRadius() {
        return radius;
    }

    public boolean contains(MyPoint p) {
        return center.distance(p)<=radius;
    }

    public boolean intersects(Circle that) {
        return this.center.distance(that.center) <= this.radius+that.radius;
    }
}
