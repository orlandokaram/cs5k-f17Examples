package com.okaram.objects;


public class Fraction {
    private int numerator, denominator;

    public Fraction(int p_numerator, int p_denominator) {

        this.numerator=p_numerator;
        this.denominator=p_denominator;

        normalize();
    }

    static int gcd(int num, int den) {
        if(den==0) {
            return num;
        } else {
            return gcd(den, num%den);
        }
    }

    private void normalize() {
        if(this.denominator==0) {
            this.denominator=1;
        }

        if(this.denominator<0) {
            this.denominator=-this.denominator;
            this.numerator=-this.numerator;
        }
        int theGcd=gcd(Math.abs(this.numerator),Math.abs(this.denominator));
        this.numerator=this.numerator/theGcd;
        this.denominator=this.denominator/theGcd;
    }

    public Fraction(int p_numerator) {
        this(p_numerator,1);
    }

    public int getNumerator() {
        return this.numerator;
    }

    public int getDenominator() {
        return this.denominator;
    }

    public double getValue() {
        return this.numerator / (double) this.denominator;
    }

    public String toString() {
        return numerator+"/"+denominator;
    }
}
