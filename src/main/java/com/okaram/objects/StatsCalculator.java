package com.okaram.objects;


public class StatsCalculator {
    private double sum=0, sumSquares=0;
    private int numValues=0;

    public void addValue(double value) {
        this.sum+=value;
        this.sumSquares+=(value*value);
        ++this.numValues;
    }

    public int getNumberOfValues() {
        return this.numValues;
    }

    public double getMean() {
        return sum/numValues;
    }

    public double getVariance() {
        return (sumSquares/numValues) - (getMean()*getMean());
    }

    public double getStandardDeviation() {
        return Math.sqrt(getVariance());
    }

    public String toString() {
        return "[ I'm a StatsCalculator, sum="+sum +" numValues="+numValues+" ]";
    }

}
