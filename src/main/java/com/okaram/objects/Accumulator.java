package com.okaram.objects;


public class Accumulator {
    private double value;

    public Accumulator(double value) {
        this.value=value;
    }

    public double getValue() {
        return this.value;
    }

    public void add(double otherValue) {
        this.value += otherValue;
    }

    public void subtract(double otherValue) {
        this.value -= otherValue;
    }

    public void multiply(double otherValue) {
        this.value *= otherValue;
    }

    public void divide(double otherValue) {
        this.value /= otherValue;
    }

}
