package com.okaram.objects;


public class Person {
    private String firstName;
    private String lastName;
    private int age;

    static private int numPeople=0;

    // constructor
    public Person(String p_firstName, String p_lastName, int p_age) {
        this.firstName=p_firstName;
        this.lastName=p_lastName;
        this.age=p_age;

        ++numPeople;
    }

    public Person(String p_firstName, String p_lastName) {
        this(p_firstName,p_lastName,18);
    }

    public Person(String p_firstName) {
        this(p_firstName,"Doe",18);
    }

    public String getFullName() {
        return this.firstName+" "+this.lastName;
    }

    public String getFormalName() {
        return lastName+", "+firstName;
    }

    public int getAge() {
        return this.age;
    }

    public static int getNumPeople() {
        return numPeople;
    }
}
