package com.okaram;

import org.w3c.dom.css.Rect;

import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Dimension;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static boolean isEven(int number)
    {
        int remainderBy2=number%2;
        return remainderBy2==0;
    }

    public static boolean isInBetween(int number, int low, int high) {
        return number>=low && number<=high;
    }

    static boolean isOdd(int number) {
        return (number % 2) != 0;
    }

    static int add(int a, int b) {
        return a+b;
    }

    static String add(String a, String b) {
        return a+b;
    }

    static double add(double a, double b) {
        return a+b;
    }

    static int add3(int a, int b, int c) {
        return add( add(a,b), c);
    }


    public static void changeInt(int a) {
        a=a+1;
    }

    public static void changePoint1(Point p) {
        p=new Point(3,5);
    }

    public static void reallyChangePoint(Point p) {
        p.x+=1;
        p.y*=2;
    }

    public static Point doublePoint(Point p) {
        p=(Point)p.clone();
        p.x*=2;
        p.y*=2;

        return p;
    }

    public static char firstCharacter(String s) {
        return s.charAt(0);
    }

    public static char lastCharacter(String s) {
        return s.charAt(s.length()-1);
    }


    public static void printTwice(String s, PrintStream out) {
        out.println( s );
        out.println( s );
    }

    public static void readLineAndPrintItTwice(Scanner in, PrintStream out) {
        String line=in.nextLine();
        printTwice(line, out);
    }


    public static String booleanToYesNo(boolean b)
    {
        if( b ) {
            return "Yes";
        } else {
            return "No";
        }
    }

    public static int abs(int number)
    {
        if(number<0) {
            return -number;
        } else {
            return number;
        }
    }

    static int sign(int number) {
        // -1 if negative, 1 if positive, 0 if 0
        if(number<0) {
            return -1;
        } else if(number>0) {
            return 1;
        } else {
            return 0;
        }
    }

    int signQM(int number) {
        return number==0 ? 0 : (number<0 ? -1 : 1);
    }

    static boolean isDivisible(int n, int m) {
        return n%m==0;
    }

    static String fizzBuzz(int number) {
        if(isDivisible(number, 3) && isDivisible(number,5)) {
            return "FizzBuzz";
        } else if(isDivisible(number, 3)) {
            return "Fizz";
        } else if (isDivisible(number,5)) {
            return "Buzz";
        } else {
            return ""+number;
        }
    }


    static String fizzBuzzSwitch(int number) {
        switch (number%15) {
            case 0:
                return "FizzBuzz";
            case 3:
            case 6:
            case 9:
            case 12:
                return "Fizz";
            case 5:
            case 10:
                return "Buzz";
            default:
                return ""+number;
        }
    }

    static int max2(int a, int b) {

        if(a>b) {
            return a;
        } else {
            return b;
        }
    }


    static int max3(int a, int b, int c) {
        return max2(a, max2(b,c));
    }

    public static void main(String[] args)
    {
        System.out.println(  max3(10,10,-1) );
    }
}
