package com.okaram;


import java.io.PrintStream;
import java.util.Scanner;

public class Loops {
    public static int readAndSum(PrintStream out, Scanner in)
    {
        int sum=0;
        int input;
        do {
            out.println("Please enter a number (0 to stop)");
            input=in.nextInt();
            sum+=input;
        } while(input!=0);
        return sum;
    }

    public static int readAndSumWhile(PrintStream out, Scanner in)
    {
        int sum=0;
        int input;

        out.println("Please enter a number (0 to stop)");
        input=in.nextInt();
        sum=sum+input;
        while(input!=0) {
            out.println("Please enter another number (0 to stop)");
            input=in.nextInt();
            sum+=input;
        }
        return sum;
    }

    static void countAndPrintWhile(int low, int high, PrintStream out) {
        int i=low;
        while(i<=high) {
            out.println(i);
            ++i;
        }
    }

    static void countAndPrintFor(int low, int high, PrintStream out) {

        for( int i=low; i<=high; ++i) {
            out.println(i);
        }
    }


    public static int readAndSumBreak(PrintStream out, Scanner in)
    {
        int sum=0;
        int input;

        while( true ) {
            out.println("Please enter a number (-1 to stop)");
            input=in.nextInt();
            if(input == -1) {
                break;
            }
            sum += input;
        }
        return sum;
    }

    static boolean isEven(int n) {
        return n%2==0;
    }

    public static int readAndSumEven(PrintStream out, Scanner in)
    {
        int sum=0;
        int input;

        while( true ) {
            out.println("Please enter a number (-1 to stop)");
            input=in.nextInt();
            if(input == -1) {
                break;
            }
            if(!isEven(input)) {
                continue;
            }
            sum += input;
        }
        return sum;
    }


    int power(int base, int exponent) {
        int pow=1;
        for(int i=0; i<exponent; ++i) {
            pow *= base;
        }
        return pow;
    }

    static int digitToNumber(char c) {
        if(c<'0' || c>'9') {
            return 0;
        }

        return c-'0';
    }

    // calculates the sum of digits
    static int sumDigits(String digits) {
        int sum=0;
        for(int i=0; i<digits.length(); ++i) {
            sum +=  digitToNumber( digits.charAt(i) );
        }
        return sum;
    }

    static int factorial(int n) {
        int fac=1;
        for( int i=2; i<=n; ++i) {
            fac *= i;
        }
        return fac;
    }

    static int fibo(int n) {
        int prev1=0, prev2=1;
        int ans=0;
        for(int i=0; i<n; ++i) {
            ans=prev1+prev2;
            prev2=prev1;
            prev1=ans;
        }
        return ans;
    }

    static void printStars(PrintStream out, int n) {
        for(int i=0; i<n;++i) {
            out.print('*');
        }
        out.println();
    }

    static void printTriangle(PrintStream out, int n) {
        for(int i=0; i<n; ++i) {
            printStars(out,i);
        }
    }

    static void printMultHeader(int n ) {
        for(int i=1; i<=n; ++i) {
            System.out.print("\t" + i);
        }
        System.out.println();
    }
    static void printMultiplicationTable(int n) {
        printMultHeader(n);
        for(int row=1; row<=n; ++row) {
            System.out.print(row+"\t");
            for(int col=1; col<=n; ++col) {
                System.out.print(row*col+"\t");
            }
            System.out.println();
        }
    }


    public static void main(String args[]) {
        Scanner sc=new Scanner(System.in);
        printMultiplicationTable(5);
    }

}
