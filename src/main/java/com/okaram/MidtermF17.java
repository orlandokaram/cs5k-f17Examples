package com.okaram;


import java.util.Scanner;

public class MidtermF17 {

    public static void q1() {
        boolean amISmart = true;
    }

    public static boolean isOdd(int n) {
        return (n % 2 == 1);
    }

    static void q2() {
        int a = 5, b;
        a = a + 5;
        b = a + 4;
        int ans = a + b;
        System.out.println(ans);
    }

    /*
    Write a function called classifyAge that takes an int, say age, and returns a string;
     the function returns
        “kid” if age is between 0 and 12 (all ranges inclusive),
        “teen” if between 13 and 19,
        “adult” if between 20 and 65,
        “senior” if between 65 and 120, and
        “invalid” if not in any of these categories.
     */
    static String classifyAge(int age) {
        if (age >= 0 && age <= 12) {
            return "kid";
        } else if (age >= 13 && age <= 19) {
            return "teen";
        }else if (age >= 20 && age <= 65) {
            return "adult";
        } else if (age >= 65 && age <= 120) {
            return "senior";
        } else {
            return "invalid";
        }
    }



    static double power_half(double base, int exponent) {
        if(exponent<=0) {
            return 1;
        } else {
            double halfPow=power_half(base, exponent/2);
            if( isOdd(exponent)) {
                return halfPow*halfPow*base;
            } else {
                return halfPow*halfPow;
            }
        }
    }

    static int power(int base, int exponent) {
        if(exponent<=0) {
            return 1;
        } else {
            return base*power(base, exponent-1);
        }
    }

    static String stringTimes(String s, int times) {
        if(times<=0) {
            return "";
        } else {
            return s.concat( stringTimes(s,times-1));
        }
    }

    static char first(String s) {
        return s.charAt(0);
    }

    static String rest(String s) {
        return s.substring(1);
    }

    static boolean stringContains(String s, char c) {
        if(s.equals("")) {
            return false;
        }
        else if(first(s)==c) {
           return true;
        } else {
            return stringContains(rest(s),c);
        }
    }

    static boolean stringContains2(String s, char c, int currentPlace) {
        if(currentPlace<0) {
            return false;
        } else if (s.charAt(currentPlace) == c) {
            return true;
        } else {
            return stringContains2(s,c,currentPlace-1);
        }
    }

    static boolean stringContains2(String s, char c) {
        return stringContains2(s,c,s.length()-1);
    }

    public static int foo(int a) {
        if (a <= 0)
            return 3;
        else
            return a * foo(a - 2);
    }

    public int readWithin0_100(Scanner in) {
        System.out.println("Please enter a number between 0 and 100");
        int userInput = in.nextInt();
        if (userInput >= 0 && userInput <= 100) {
            return userInput;
        } else {
            return readWithin0_100(in);
        }
    }


    public static void main(String args[]) {
        double d=122.2;
        int i=33;
        String word="hello";

        System.out.printf("The string %% is %s, int is %x %2.2f", word, i, d);
    }

}
