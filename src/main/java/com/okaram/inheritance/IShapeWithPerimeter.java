package com.okaram.inheritance;


public interface IShapeWithPerimeter {
    double getPerimeter();
}
