package com.okaram.inheritance;


public interface IShapeWithArea {
    double getArea();
}
