package com.okaram.inheritance;


import java.io.PrintStream;

public class Main {


    static void printAreaAndPerimeter(PrintStream out, IShape s) {
        out.println("For "+s + "Its area is "+s.getArea() +
                " and its perimeter is "+s.getPerimeter());
    }

    public static void printAll(PrintStream out, IShape[] shapes) {
        for(IShape shape : shapes) {
            printAreaAndPerimeter(out,shape);
        }
    }

    public static void main(String[] args) {
        Rectangle r=new Rectangle(1.0,2.0);
        Circle c=new Circle(2);
        Parallelogram p=new Parallelogram(2,3,4);

        IShape[] shapes={r,c,p};

        printAll(System.out, shapes);
    }

}
