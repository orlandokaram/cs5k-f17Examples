package com.okaram.inheritance;


public class Rectangle implements IShape {
    private double width, height;
    public Rectangle(double width, double height) {
        this.width=width;
        this.height=height;
    }

    public double getArea() {
        return width*height;
    }
    public double getPerimeter() {
        return 2*(width+height);
    }

    public String toString(){
        return "Rectangle[width:"+width+" height:"+height+"]";
    }
}
