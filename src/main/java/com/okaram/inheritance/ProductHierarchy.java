package com.okaram.inheritance;


public class ProductHierarchy {

    public class Person {
        public String firstName,lastName;
    }
    static class Product {
        public String id;
        public String description;
        public double price;
    }

    static class Book extends Product {
        public String ISBN10, ISBN13;
        public String title;
        Person[] authors;
    }

    static class CD extends Product {
        String[]songs;
        String title;
        String barcode;
        Person[] performers;
    }


    static double calculateTotal(Product[] products) {
        return 0;
    }
}
