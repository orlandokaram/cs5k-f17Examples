package com.okaram.inheritance;


public class Parallelogram implements IShape {
    private double base, height, a;

    public Parallelogram(double base, double height, double a) {
        this.base=base;
        this.height=height;
        this.a=a;
    }

    @Override
    public double getArea() {
        return base*height;
    }

    @Override
    public double getPerimeter() {
        return 2*(base+a);
    }

    @Override
    public String toString() {return "Some parallelogram";}

    public double getBase() {return base;}
}
