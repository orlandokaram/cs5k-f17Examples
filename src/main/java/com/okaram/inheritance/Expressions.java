package com.okaram.inheritance;


public class Expressions {

    interface Expression {
        int eval();
    }

    static class Literal implements Expression {
        int value;
        public Literal(int value) {
            this.value=value;
        }

        public int eval() {
            return value;
        }
    }

    static class UnaryOperator implements Expression {
        Expression operand;
        public UnaryOperator(Expression operand) {
            this.operand=operand;
        }

        protected int operate(int value) {
            return value;
        }

        public int eval() {
            return operate(  operand.eval() );
        };
    }

    static class UnaryMinus extends UnaryOperator {
        UnaryMinus(Expression operand) {
            super(operand);
        }

        @Override
        protected int operate(int value) {
            return -value;
        }
    }

    static class UnaryPlus extends UnaryOperator {
        UnaryPlus(Expression operand) {
            super(operand);
        }
    }

    static class Abs extends UnaryOperator {
        public Abs(Expression operand) {
            super(operand);
        }

        @Override
        protected int operate(int n) {
            return n>0 ? n : -n;
        }
    }



    public abstract static class BinaryOperator implements Expression {
        Expression left, right;
        public BinaryOperator(Expression left, Expression right) {
            this.left=left;
            this.right=right;
        }

        abstract protected int operate(int l, int r);

        public int eval() {
            return operate(    left.eval(),   right.eval() );
        }
    }

    public static class BinaryPlus extends BinaryOperator {

        public BinaryPlus(Expression left, Expression right ) {
            super(left,right);
        }

        @Override
        protected int operate(int l, int r) {
            return l+r;
        }
    }

    public static void main(String[] args) {

        Expression e=new BinaryPlus( new Literal(3), new UnaryMinus(new Literal(5)) );

        System.out.println(e.eval());
    }
}
