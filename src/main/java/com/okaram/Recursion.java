package com.okaram;


import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.Scanner;

public class Recursion {
    public static int factorial(int n) {
        System.out.println("Factorial n="+n);
        if(n<=0)
            return 1;
        else
            return n*factorial(n-1);
    }

    static double power(double base, int exponent) {
        if(exponent<0) {
            return 1/power(base,-exponent);
        }
        if(exponent == 0) {
            return 1;
        } else {
            return base * power(base, exponent-1);
        }
    }
    // count down, printing to out, until to
    // print to
    public static void printCountDown(PrintStream out, int from, int to)
    {
        if(from>=to) {
            out.println(from);
            printCountDown(out, from-1, to);
        }
    }

    // count up, printing to out, until to
    // print to
    public static void printCountUp(PrintStream out, int from, int to)
    {
        if(from<=to) {
            out.println(from);
            printCountUp(out, from+1, to);
        }
    }

    static boolean isEven(int n) {
        return n%2==0;
    }

    static double power_half(double base, int exponent) {
        if(exponent==0) {
            return 1;
        } else {
            double halfPow=power_half(base, exponent/2);
            if(isEven(exponent)) {
                return halfPow * halfPow;
            } else {
                return base * halfPow * halfPow;
            }
        }

    }

    // does s contain c, from index from to end of string
    static boolean stringContains(String s, char c, int from) {
        if(from >= s.length()) {
            return false;
        }
        else if(s.charAt(from) == c) {
            return true;
        }
        return stringContains(s,c,from-1);
    }


    // reads yes or no from keyboard, returns true or false
    // rereads if user enters something else
    static boolean readYesNo(PrintStream out, Scanner sc) {
        out.println("Yes or No ?");
        String answer=sc.next();
        if(  answer.toLowerCase().equals("yes")) {
            return true;
        } else if (  answer.toLowerCase().equals("no")) {
            return false;
        }
        else {
            return readYesNo(out,sc);
        }
    }

    static int square(int n) {
        return n*n;
    }

    // print squares of numbers from low to high, inclusive
    // assumes low <= high
    static void printSquares(int low, int high, PrintStream out) {
        if(low<=high) {
            out.println(square(low));
            printSquares(low + 1, high, out);
        }
    }

    static int numFibs=0;
    static int fibo(int n) {
        ++numFibs;
        if(n<=0) {
            return 0;
        } else if (n==1) {
            return 1;
        } else {
            return fibo(n-1)+fibo(n-2);
        }
    }

    static int fibo(int n, int prev1, int prev2) {
        ++numFibs;

        if(n<=0) {
            return prev2;
        } else if (n==1) {
            return prev1;
        } else {
            return fibo(n-1, prev1+prev2, prev1);
        }
    }

    static String reverse(String s) {
        if(s.equals(""))
            return "";
        char first=s.charAt(0);
        String rest=s.substring(1);
        return reverse(rest)+first;
    }

    static int countOccurrences(char c, String s) {
        if(s.equals("")) {
            return 0;
        } else {
            char first=s.charAt(0);
            String rest=s.substring(1);
            if(first==c) {
                return 1+countOccurrences(c,rest);
            } else {
                return countOccurrences(c,rest);
            }
        }
    }

    public static void main(String args[]) {
        System.out.println(countOccurrences('a',"abcdaef"));
        /*
        int n=20;
        numFibs=0;
        System.out.println(fibo(n));
        System.out.println(numFibs);
        numFibs=0;
        System.out.println(fibo(n,1,0));
        System.out.println(numFibs);
        */
    }
}
