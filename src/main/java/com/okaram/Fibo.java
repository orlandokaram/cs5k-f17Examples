package com.okaram;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Fibo {
    public static int numCalls=0;

    public static Map<Integer,Integer> cache=new HashMap<>();
    static int base_fibo(int n) {
        numCalls++;
        if(n==0) {
            return 0;
        }
        else if(n==1) {
            return 1;
        }
        else return fibo_memo(n-1)+fibo_memo(n-2);

    }
    static int fibo_memo(int n) {
        Integer cachedAnswer=cache.get(n);
        if(cachedAnswer!=null)
            return cachedAnswer;
        else {
            int answer=base_fibo(n);
            cache.put(n,answer);
            return answer;
        }
    }

    static int fibo1(int n) {
        int values[]=new int[n];
        values[0]=0;
        values[1]=1;
        for(int i=2; i<n;++i) {
            int ans=values[i-2]+values[i-1];
            values[i]=ans;
        }
        return values[n-1]+values[n-2];
    }

    public static void main(String[] args) {
        for(int i=2; i<40; ++i) {
            numCalls=0;
            System.out.print(i);
            System.out.println("\t"+fibo1(i));
        }
    }
}
