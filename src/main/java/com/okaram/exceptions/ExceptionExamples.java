package com.okaram.exceptions;


public class ExceptionExamples {

    public static class MyException extends RuntimeException {
        public MyException(String message, Exception parent) {
            super(message,parent);
        }
    }
    static int parseAndAdd(String s1, String s2) throws MyException {
        try {
            int n1 = Integer.parseInt(s1);
            int n2 = Integer.parseInt(s2);

            return n1 + n2;
        } catch (NumberFormatException e) {
            System.out.println("Warning: "+e);

            throw new MyException("Bad juju",e);
        }
    }

    public static void main(String[] args) {
        System.out.println(  parseAndAdd("1","3") );
        System.out.println(  parseAndAdd("joe","bob") );
    }
}
