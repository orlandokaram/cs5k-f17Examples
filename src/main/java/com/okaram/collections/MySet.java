package com.okaram.collections;

import java.util.ArrayList;
import java.util.List;

// set of integers
public class MySet<Type> {
    List<Type> elements=new ArrayList<>();

    public void add(Type element) {
        if(!contains(element)) {
            elements.add(element);
        }
    }

    public boolean contains(Type element) {
        for(Type elem: elements) {
            if(elem.equals(element))
                return true;
        }
        return false;
    }
}
