package com.okaram.collections;


public class Main {

    public static void main(String[] args) {
        MySet<Integer> ints=new MySet<>();
        ints.add(3);
        ints.add(5);

        System.out.println( ints.contains(5) );


        MySet<String> strings=new MySet<>();
        strings.add("three");
        strings.add(new String("five"));

        System.out.println( strings.contains("five") );
    }
}
