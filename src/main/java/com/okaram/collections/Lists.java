package com.okaram.collections;

import java.util.*;
import java.awt.Point;


public class Lists {

    public static String addAll(List<String> numbers) {

        String sum="";
        for( int i=0; i<numbers.size(); ++i) {
            sum +=numbers.get(i);
        }
        return sum;
    }

    public static String addAll2(List<String> numbers) {

        String sum="";
        for(String elem : numbers) {
            sum+=elem;
        }
        return sum;
    }

    public static void playWithLists() {
        List<String> l = new LinkedList<>();

        l.add("3");
        l.add("4");
        l.add("5");

        System.out.println(   addAll2(l)     );
    }

    public static void main(String[] args) {
        Map<String, String> translations=new HashMap<>();
        translations.put("one", "uno");
        translations.put("two", "dos");
        translations.put("three", "tres");
        translations.put("three", "trois");

        System.out.println(   translations.size()   );
        System.out.println(   translations.get("three")   );
        System.out.println(   translations.containsKey("one")   );
        System.out.println(   translations.containsKey("uno")   );

        Collection<String> keys=translations.keySet();
        Collection<String> valus=translations.values();

        Collection< Map.Entry<String,String> > entries=translations.entrySet();
        for(Map.Entry<String,String> entry: entries) {
            System.out.println(entry.getKey() + " " + entry.getValue() );
        }
    }
}
