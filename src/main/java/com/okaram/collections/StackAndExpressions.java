package com.okaram.collections;


import com.okaram.inheritance.Circle;

import java.util.HashSet;
import java.util.Set;
import java.util.Stack;

public class StackAndExpressions {

    static String reverse(String s) {
        Stack<Character> letters=new Stack<>();
        for(int i=0; i<s.length() ; ++i) {
            letters.push(s.charAt(i));
        }

        String ans="";
        while (!letters.isEmpty()) {
            ans = ans + letters.pop();
        }
        return ans;
    }


    public String operate(String operator, String n1, String n2) {
        int i1=Integer.parseInt(n1);
        int i2=Integer.parseInt(n2);

        if(operator=="+") {
            return ""+(i1+i2);
        }
        if(operator=="*") {
            return ""+(i1*i2);
        }
        if(operator=="-") {
            return ""+(i1-i2);
        }
        return "";
    }

    public String evaluateExpression(Stack<String> expr) {
        Set<String> operators=new HashSet<String>() ;
        operators.add("+");
        operators.add("-");
        operators.add("*");

        int accum=0;

        String top=expr.pop();
        if(operators.contains(top)) {
            String n1=expr.pop();
            String n2=expr.pop();
            String val=operate(top,n1,n2);
        }
        return "";
    }
    public static void main(String[] args) {
        System.out.println(   reverse("abc123"));
    }
}
