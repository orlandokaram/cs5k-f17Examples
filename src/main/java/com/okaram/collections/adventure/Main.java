package com.okaram.collections.adventure;


import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Page p1=new Page(1,"You're in the surface of the sea, next to the beach.");

        p1.addAction("go under water", 2);
        p1.addAction("get out and go to the beach", 3);

        Page p2=new Page(2, "You run out of air and die");
        Page p3=new Page(3, "You get ice cream and are happy");

        Book b=new Book("Going to the beach",1);
        b.addPage(p1);
        b.addPage(p2);
        b.addPage(p3);

        b.successPages.add(3);
        b.failurePages.add(2);

        System.out.println(   b.play( new Scanner(System.in), System.out) );
    }
}
