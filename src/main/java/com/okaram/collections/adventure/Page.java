package com.okaram.collections.adventure;


import java.util.HashMap;
import java.util.Map;

public class Page {
    public int number;
    public String text;

    Map<String, Integer> possibleActions;

    public Page(int number, String text) {
        this.number=number;
        this.text=text;
        possibleActions=new HashMap<>();
    }

    public void addAction(String description, int targetPage) {
        possibleActions.put(description,targetPage);
    }
}
