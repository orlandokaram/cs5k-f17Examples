package com.okaram.collections.adventure;


import java.io.PrintStream;
import java.io.Reader;
import java.util.*;

public class Book {
    String title;

    Map<Integer, Page> pages;

    Set<Integer> successPages=new HashSet<>();
    Set<Integer> failurePages=new HashSet<>();

    int startPage;

    public Book(String title, int startPage) {
        this.title=title;
        this.startPage=startPage;
        pages=new HashMap<>();
    }

    public void addPage(Page p) {
        pages.put(p.number, p);
    }

    boolean play(Scanner in, PrintStream out) {
        int currentPageNumber=startPage;

        while(!successPages.contains(currentPageNumber) && !failurePages.contains(currentPageNumber)) {
            Page currentPage=pages.get(currentPageNumber);

            out.println(currentPage.text);

            for(Map.Entry<String,Integer> entry : currentPage.possibleActions.entrySet()) {
                out.println(entry.getKey() + " to go to page " + entry.getValue());
            }
            out.println("Please enter the page number you want to go");
            int nextPage=in.nextInt();
            while (!currentPage.possibleActions.values().contains(nextPage)) {
                out.println("Sorry, invalid page");
                nextPage=in.nextInt();
            }
            currentPageNumber=nextPage;
        }

        out.println(pages.get(currentPageNumber).text);

        return successPages.contains(currentPageNumber);
    }
}
