package com.okaram.finalish;

/*
Write a class called Fraction with the following methods/constructor:

A constructor that takes two int parameters, the numerator and denominator, respectively
a method called getNumerator, that returns an int, the numerator
a method called getDenominator, that returns an int, the denominator
a method called toDouble, that returns a double, the equivalent decimal to the fraction.
 */
public class Fraction {
    private int numerator;
    private int denominator;
    public  Fraction(int numerator, int denominator){
        this.numerator = numerator;
        this.denominator = denominator;

    }
    public int getNumerator(){
        return numerator;
    }
    public int getDenominator(){
        return denominator;
    }
    public double toDouble () {
        return numerator /(double) denominator;
    }


}
