package com.okaram.finalish;


public interface Product {
    public String getName();
    public String getType();
    public double getPrice();
}
