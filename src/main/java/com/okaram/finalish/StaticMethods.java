package com.okaram.finalish;


import javax.validation.groups.ConvertGroup;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StaticMethods {

    /*Given the Product interface above, write a function called AveragePrice, that takes an
    array of Product, and returns a double, the average price of the product.
     */

    public static double averagePrice(Product[] products) {
        double sumPrices=0;
        for(Product p : products) {
            sumPrices+=p.getPrice();
        }
        return sumPrices / products.length;
    }

    /*Given the Product interface above, write a function called HighestPricedProduct, that
    takes a List of Product, and returns a Product, the product with the highest price.
     */
    public Product highestPricedProduct(List<Product> productList) {
        Product highestPriced=productList.get(0);
        for(Product p : productList) {
            if(p.getPrice() > highestPriced.getPrice()) {
                highestPriced = p;
            }
        }
        return highestPriced;
    }
/*
    Java has an Integer class, with a static method parseInt, that takes a string and returns
    an int. This function throws a NumberFormatException if the string does not represent a valid
    int. Write a static function called parseInt, which takes two arguments, a String and an int
    (the int represents the default value), and returns an int. This function calls
    Integer.parseInt, and handles the exception so that, if parseInt raises a
    NumberFormatException the function would return the default value.
    */

    public static int parseInt(String numericString, int defaultValue) {
        try {
            return Integer.parseInt(numericString);
        } catch (NumberFormatException nfe) {
            return defaultValue;
        }
    }

    /*
    Given the Fraction class you created above, write a static createFraction method, that takes
    two integers, the numerator and denominator, and returns a Fraction with that numerator
    and denominator; however, if the denominator is zero, the function will throw an
    IllegalArgumentException (this class is defined by java and has a constructor with no
    arguments and one which takes a String argument for an error message, if you prefer).
     */
    public static Fraction createFraction(int num, int den) throws IllegalArgumentException{
        if(den==0) {
            throw new IllegalArgumentException("Denominator cannot be 0");
        }
        return new Fraction(num,den);
    }
    /*
    Write a function called ArrayFrequencies, that takes a List of Strings, and returns a Map of String
    to Integer. The function maps every String in the array to its frequency (so if the string appears 3 times,
     it would be in the map paired with a value of 3). Hint- HashMap is the standard class for Maps.
     Map has methods containsKey, put and get.
     */
    public static Map<String,Integer> arrayFrequencies(List<String> words) {
        Map<String,Integer> frequencies=new HashMap<>();

        for(String word : words) {
            Integer frequency=frequencies.get(word);
/*            if(frequency==null) {
                frequency = 1;
            }
            else {
                frequency = frequency + 1;
            }
*/
            frequency= (frequency==null? 0 : frequency) + 1;
            frequencies.put(word, frequency);
        }

        return frequencies;
    }
/*
    Write a function called MostCommonWord, that takes a List of Strings and returns the String which occurs
    more frequently in the list. You can assume the array contains no null values.
    Hint-Can call function above. Map also has methods keySet and entrySet which may be useful.
    EntrySet returns a Collection of Map.Entry ; Map.Entry has getKey() and getValue().
*/
    public static String mostCommonWord(List<String> words) {
        Map<String,Integer> frequencies=arrayFrequencies(words);
        Integer maxFreq=0;
        String maxString="";

        for(Map.Entry<String,Integer> entry : frequencies.entrySet() ) {
            if(entry.getValue() > maxFreq) {
                maxFreq=entry.getValue();
                maxString=entry.getKey();
            }
        }
        return maxString;
    }

    static int max3(int a1, int a2, int a3) {
        int max=a1;

        if(a2>max) {
            max=a2;
        }

        if(a3>max) {
            max=a3;
        }

        return max;
    }

    /*
    Map<String,Integer> m1=new HashMap<>();
    m1.put("High",10);
     */
}
