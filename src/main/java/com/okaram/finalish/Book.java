package com.okaram.finalish;


public class Book implements Product {

    private String name;
    private double price;
    private static final String type="Book";

    public Book(String name, double price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public double getPrice() {
        return price;
    }

    /* Example use (q3)
    Write a Java code fragment to create a variable called b1, of type Book
    and assign it to a Book instance with name "Catch 22" and price 7.95

    Book b1=new Book("Catch 22",7.95);
     */
}
