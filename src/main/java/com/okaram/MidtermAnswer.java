package com.okaram;


import java.io.PrintStream;

public class MidtermAnswer {

    /*
    Using recursion (no loops allowed) write a function called PrintByStep, that takes two
    parameters, a PrintStream and an int (say, step). The function prints to the printstream
    all numbers from 0 to 100, incrementing by step (so, for example,
    PrintByStep(System.out, 20) would print 0,20,40,60,80,100)
    */
    static void printByStep(PrintStream out, int step) {
        printByStep(out,step,0);
    }

    static void printByStep(PrintStream out, int step, int current) {
        if(current<=100) {
            out.println(current);
            printByStep(out,step, current+step);
        }
    }

    static void printByStep2(PrintStream out, int step, int count) {
        if(count*step<=100) {
            out.println(count*step);
            printByStep(out,step, count+1);
        }
    }

    static void printByStepLoopy(PrintStream out, int step ) {
        for(int current=0; current<=100; current+=step) {
            out.println(current);
        }
    }

    /*
    (Sunday) Using recursion (no loops allowed) write a function called PrintByStep,
    that takes three parameters, a PrintStream and an two ints(say, low and high).
    The function prints to the printstream all numbers from low  to high,
    incrementing by 5 (so, for example, PrintByStep(System.out, 1,15) would print 1,6,11).
    You may need to define a recursion function with extra parameters and call it.
     */
    static void printByStepSunday(PrintStream out, int low, int high) {
        if (low <= high) {
            out.println(low);
            printByStepSunday(out, low + 5, high);
        }
    }



    static char first(String s) {
        return s.charAt(0);
    }

    static String rest(String s) {
        return s.substring(1);
    }
    /*
    Using recursion (no loops allowed) write a function called stringContains that takes two
     parameters, a String and a char ; stringContains returns true if the string contains
     the character, false otherwise.
     Hint - You can assume there are functions first, that takes a string and returns a char,
      and rest, that take a string and returns the string without the first character.
     Alternatively, define a function that takes 3 parameters and use charAt and length
     */
    static boolean stringContains(String s, char c) {
        if(s.isEmpty()) {
            return false;
        } else if(first(s) == c) {
            return true;
        } else {
            return stringContains(rest(s),c);
        }
    }

    static boolean stringContainsWhile(String s, char c) {
        while(!s.isEmpty()) {
            if(first(s)==c)
                return true;
            s=rest(s);
        }
        return false;
    }

    static boolean stringContains(String s, char c, int index) {
        if(index>=s.length()) {
            return false;
        } else if (s.charAt(index)==c ) {
            return true;
        } else {
            return stringContains(s,c,index+1);
        }
    }

    static boolean stringContainsFory(String s, char c, int index) {
        for(int i=0; i<s.length(); ++i) {
            if(s.charAt(i)==c) {
                return true;
            }
        }
        return false;
    }

    public static void main(String[] args) {
        System.out.println(stringContains("abc",'c'));
        System.out.println(stringContains("abc",'d'));
    }
}
